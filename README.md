
[![](https://discordapp.com/api/guilds/96753964485181440/widget.png?style=banner2)](https://discord.gg/ngNQjT5) [![](https://s12.directupload.net/images/200916/joj33k55.png)](https://twitter.com/kreezxil) [![](https://s12.directupload.net/images/200916/efhmdjhg.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.reddit.com%252fr%252fMinecraftModdedForge)

[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

## Videos

## Screens

[![](https://i.imgur.com/4OV0KIE.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fi.imgur.com%252f4OV0KIE.png)

[![](https://i.imgur.com/vfjM63f.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fi.imgur.com%252fvfjM63f.png)

Click Images for Larger View

Spoiler (click to show)

![](https://www.youtube.com/watch?v=-FrEBqm7PzQ)

## Description

---

A lever light? Is that a light switch? I've also created a Glass block light! The block lights itself when you hit it and if you hit again, it turns off. If it is on, it'll send a redstone signal. So if you only wanted regular glass, use that.

Added the awesome solid textures from Extra Mods and created 16 new buttons that are either on or off like a lever. They emit a level 15 redstone signal when on, otherwise 0. When on they look beautiful.

Recipes

---

![One Stick, One Glass Block](https://media-elerium.cursecdn.com/attachments/214/969/glass-lever-light-recipe.png) ![One Torch, One Glass Block](https://media-elerium.cursecdn.com/attachments/214/973/glass-block-light-recipe.png)

The new colored button lights are formed by placing 1 colored stained glass in a crafting grid.

TODO

---

1.  Adjust hit box models to more tight around the buttons and large block light.
2.  Implement an Albedo library to have real colored light emanate from the light sources.

## Modpacks

---

No need to ask. Add it. A link back would be appreciated but not required.

## Help a Veteran today

---

I am Veteran of United States Army. I am not disabled. But I do love to make these mods and modpacks for you guys. Please help me to help you by Donating at [https://patreon.com/kreezxil](https://patreon.com/kreezxil)

**This project is proudly powered by FORGE**, without whom it would not be possible. Help FORGE get rid of Adfocus at [https://www.patreon.com/LexManos](https://www.patreon.com/LexManos).
